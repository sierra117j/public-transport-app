<?php

class Bus
{
    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    }

    public function newBus($data)
    {
        try {
            if ($this->pdo->insert('bus', $data)) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function updateBus($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];
            $this->pdo->update('bus', $data, $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $strSql = "SELECT * FROM bus INNER JOIN user ON bus.id_user = user.id INNER JOIN role on user.id_role = role.id";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
