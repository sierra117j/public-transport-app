<?php

class Driver
{
    private $id;
    private $identification;
    private $first_name;
    private $last_name;
    private $id_role;
    private $pdo;


    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $strSql = "SELECT * FROM users WHERE id_role = 2";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }


    public function newDriver($data)
    {
        try {
            if ($this->pdo->insert('users', $data)) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function updateDriver($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];
            if ($this->pdo->update('users', $data, $srtWhere)) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function deleteDriver($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];
            if ($this->pdo->delete('users', $srtWhere)) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getById($id)
    {
        try {
            $strSql = 'SELECT * FROM users WHERE id = :id';
            $array = ['id' => $id];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
