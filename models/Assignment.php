<?php

class Assignment
{
    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    }

    public function newAssignment($data)
    {
        try {
            if ($this->pdo->insert('assignment', $data)) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function updateAssignment($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];
            $this->pdo->update('assignment', $data, $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $strSql = "SELECT * FROM assignment INNER JOIN user ON assignment.id_user = user.id INNER JOIN bus on assignment.id_bus = bus.id";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
