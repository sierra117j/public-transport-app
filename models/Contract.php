<?php

class Contract
{
    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    }

    public function newContract($data)
    {
        try {
            if ($this->pdo->insert('contract', $data)) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function renoveConctract($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];
            $this->pdo->update('contract', $data, $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getById($id)
    {
        try {
            $strSql = 'SELECT * FROM contract WHERE id = :id';
            $array = ['id' => $id];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $strSql = "SELECT * FROM contracts INNER JOIN users ON contracts.id_user = users.id INNER JOIN roles on users.id_role = roles.id";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
