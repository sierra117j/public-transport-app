<?php

class Role
{
    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $strSql = "SELECT * FROM role";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }


    public function insert($data)
    {
        try {
            $this->pdo->insert('role', $data);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function update($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];
            $this->pdo->update('role', $data, $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getById($id)
    {
        try {
            $strSql = 'SELECT * FROM role WHERE id = :id';
            $array = ['id' => $id];
            $query = $this->pdo->select($strSql, $array);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
