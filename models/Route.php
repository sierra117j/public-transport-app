<?php

class Route
{
    public function __construct()
    {
        try {

            $this->pdo = new Database;
        } catch (PDOException $e) {

            die($e->getMessage());
        }
    }

    public function newRoute($data)
    {
        try {
            if ($this->pdo->insert('route', $data)) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function updateRoute($data)
    {
        try {
            $srtWhere = 'id = ' . $data['id'];
            $this->pdo->update('route', $data, $srtWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $strSql = "SELECT * FROM route INNER JOIN user ON route.id_user = user.id INNER JOIN assignment on assignment.id_user = user.id INNER JOIN bus ON assignment.id_bus = bus.id";
            $query = $this->pdo->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
