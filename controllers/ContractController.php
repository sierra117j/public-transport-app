<?php

require 'models/Contract.php';
require 'models/Driver.php';


class ContractController
{
    private $contract;
    private $driver;

    public function __construct()
    {
        $this->contract = new Contract();
        $this->driver = new Driver();
    }

    public function index()
    {
        require 'views/layout.php';
        $contracts = $this->contract->getAll();
        require 'views/pages/contract/list.php';
    }
    public function add()
    {
        require 'views/layout.php';
        $drivers = $this->driver->getAll();
        require 'views/pages/contract/add.php';
    }

    public function save()
    {
        if (isset($_POST)) {
            if ($this->contract->newContract($_POST)) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        } else {
            echo "Error";
        }
    }
}
