<?php

require 'models/Driver.php';
require 'models/Role.php';

class DriverController
{
    private $driver;


    public function __construct()
    {
        $this->driver = new Driver();
    }

    public function index()
    {
        require 'views/layout.php';
        $drivers = $this->driver->getAll();
        require 'views/pages/driver/list.php';
    }
    public function add()
    {
        require 'views/layout.php';
        require 'views/pages/driver/add.php';
    }
    public function save()
    {
        if (isset($_POST)) {
            $_POST['id_role'] = 2;
            if ($this->driver->newDriver($_POST)) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        } else {
            echo "Error";
        }
    }

    public function edit()
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $data = $this->driver->getById($id);

            require 'views/layout.php';
            require 'views/pages/driver/edit.php';
        } else {
            echo "Error";
        }
    }

    public function update()
    {
        if (isset($_POST)) {
            if ($this->driver->updateDriver($_POST)) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        } else {
            echo "Error";
        }
    }

    public function delete()
    {
        if (isset($_GET)) {
            if ($this->driver->deleteDriver($_GET)) {
                header('location: ?controller=driver');
            } else {
                echo 'Error deleting Driver';
            }
        } else {
            echo 'Error';
        }
    }
}
