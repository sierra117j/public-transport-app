<?php

require 'models/Bus.php';


class BusController
{
    private $bus;

    public function __construct()
    {
        $this->bus = new Bus();
    }

    public function save()
    {
        if (isset($_POST)) {
            if ($this->bus->newBus($_POST)) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        } else {
            echo "Error";
        }
    }

    public function update()
    {
        if (isset($_POST)) {
            if ($this->bus->updateBus($_POST)) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        } else {
            echo "Error";
        }
    }
}
