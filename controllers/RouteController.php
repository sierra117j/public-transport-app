<?php

require 'models/Route.php';


class RouteController
{
    private $route;

    public function __construct()
    {
        $this->route = new Route();
    }

    public function save()
    {
        if (isset($_POST)) {
            if ($this->route->newRoute($_POST)) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        } else {
            echo "Error";
        }
    }

    public function update()
    {
        if (isset($_POST)) {
            if ($this->route->updateRoute($_POST)) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false]);
            }
        } else {
            echo "Error";
        }
    }
}
