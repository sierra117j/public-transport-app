function addAssignment(){
    let bus = $('#bus').val();
    let user = $('#user').val();

    let Data = {
        'id_bus': bus,
        'id_user': user,
    }

    $.ajax({
        url:"?controller=assignment&method=save",
        type:"POST",
        data:Data,
        success:function(response)
        {
            let {success} = JSON.parse(response);
            console.log(success);
            if(success){
                alert("Asignación registrada");
                window.location = '?controller=assignment';
            }
            else{
                alert("Error agregando la asignación");
            }
        }
      });
}


$(document).ready(function() {  	
	$('#formAssignment').submit(function(e) {
    e.preventDefault();
	});
	$('#btnSaveAssignment').click(function(e) {
    e.preventDefault();
		addAssignment();
	});
});