function addBus(){
    let brand = $('#brand').val();
    let plate = $('#plate').val();

    let Data = {
        'brand': brand,
        'plate': plate,
    }

    $.ajax({
        url:"?controller=bus&method=save",
        type:"POST",
        data:Data,
        success:function(response)
        {
            let {success} = JSON.parse(response);
            console.log(success);
            if(success){
                alert("Bus registrado");
                window.location = '?controller=bus';
            }
            else{
                alert("Error agregando Bus");
            }
        }
      });
}


$(document).ready(function() {  	
	$('#formBus').submit(function(e) {
    e.preventDefault();
	});
	$('#btnSaveBus').click(function(e) {
    e.preventDefault();
		addBus();
	});
});