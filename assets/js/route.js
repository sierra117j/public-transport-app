function addRoute(){
    let route = $('#route').val();
    let nameRoute = $('#nameroute').val();
    let observationRoute = $('#observationroute').val();

    let Data = {
        'route': route,
        'name': nameRoute,
        'observation': observationRoute,
    }

    $.ajax({
        url:"?controller=route&method=save",
        type:"POST",
        data:Data,
        success:function(response)
        {
            let {success} = JSON.parse(response);
            console.log(success);
            if(success){
                alert("Ruta registrada");
                window.location = '?controller=route';
            }
            else{
                alert("Error agregando ruta");
            }
        }
      });
}


$(document).ready(function() {  	
	$('#formRoute').submit(function(e) {
    e.preventDefault();
	});
	$('#btnSaveRoute').click(function(e) {
    e.preventDefault();
		addRoute();
	});
});