function addContract(){
    let start_date = $('#start_date').val();
    let final_date = $('#final_date').val();
    let id_user = $('#driver option:selected').val();
    let total_value = $('#total_value').val();

    let Data = {
        'start_date': start_date,
        'final_date': final_date,
        'id_user':id_user,
        'total_value': total_value,
    }

    $.ajax({
        url:"?controller=contract&method=save",
        type:"POST",
        data:Data,
        success:function(response)
        {
            let {success} = JSON.parse(response);
            console.log(success);
            if(success){
                alert("Contrato registrado");
                window.location = '?controller=contract';
            }
            else{
                alert("Error agregando contrato");
            }
        }
      });
}


$(document).ready(function() {  	
	$('#formContract').submit(function(e) {
    e.preventDefault();
	});
	$('#btnSaveContract').click(function(e) {
    e.preventDefault();
		addContract();
	});
});