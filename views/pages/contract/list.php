<main>
    <div class="container py-5 text-center">
        <h1>Contratos</h1>
        <div class="mx-auto my-5 col-12 flex-nowrap">
            <div class="d-flex justify-content-between">
                <a class="btn btn-success m-2" href="?controller=contract&method=add"> Agregar <i class="fas fa-plus fa-sm"></i></a>
            </div>
            <table class="table table-bordered table-striped table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Fecha de inicio</th>
                        <th>Fecha de terminacion</th>
                        <th>Nombres del conductor</th>
                        <th>Apellidos del conductor</th>
                        <th>valor total</th>
                        <th>Opciones</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($contracts as $contract) : ?>
                        <tr>
                            <td> <?php echo $contract->id ?> </td>
                            <td> <?php echo $contract->start_date ?> </td>
                            <td> <?php echo $contract->final_date ?></td>
                            <td> <?php echo $contract->first_name ?></td>
                            <td> <?php echo $contract->last_name ?></td>
                            <td> $<?php echo $contract->total_value ?></td>
                            <td>
                                <a class="btn btn-primary mr-1" href="?controller=contract&method=edit&id=<?php echo $contract->id ?>"> <i class="far fa-edit"></i></a>
                                <a class="btn btn-danger mr-2" href="?controller=driver&method=delete&id=<?php echo $contract->id ?>"> <i class="far fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</main>