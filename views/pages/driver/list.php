<main>
    <div class="container py-5 text-center">
        <h1>Conductores</h1>
        <div class="mx-auto my-5 col-10 flex-nowrap">
            <div class="d-flex justify-content-between">
                <a class="btn btn-success m-2" href="?controller=driver&method=add"> Agregar <i class="fas fa-plus fa-sm"></i></a>
            </div>
            <table class="table table-bordered table-striped table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Identificacion</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Opciones</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($drivers as $driver) : ?>
                        <tr>
                            <td> <?php echo $driver->id ?> </td>
                            <td> <?php echo $driver->identification ?> </td>
                            <td> <?php echo $driver->first_name ?></td>
                            <td> <?php echo $driver->last_name ?></td>

                            <td>
                                <a class="btn btn-primary mr-1" href="?controller=driver&method=edit&id=<?php echo $driver->id ?>"> <i class="far fa-edit"></i></a>

                                <a class="btn btn-danger mr-2" href="?controller=driver&method=delete&id=<?php echo $driver->id ?>"> <i class="far fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</main>