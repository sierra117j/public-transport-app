<main class="container py-5">

    <div class="row">
        <div class="col-12 text-center mb-5">
            <h1>Conductor</h1>
        </div>
    </div>

    <section class="row d-flex justify-content-center">
        <div class="col-4">
            <div class="card shadow-lg">
                <div class="card-header">
                    <b>Editar Conductor</b>
                </div>

                <div class="card-body">
                    <form id="formDriver">
                        <input type="hidden" id="id" class="form-control" value="<?php echo $data[0]->id; ?>">
                        <div class="form-group">
                            <label for="firstname">Identificación</label>
                            <input id="identification" type="text" class="form-control" placeholder="Ingrese Identificacion" value="<?php echo $data[0]->identification ?>">
                        </div>
                        <div class=" form-group">
                            <label for="firstName">Nombres</label>
                            <input id="firstName" type="text" class="form-control" placeholder="Ingrese Nombres" value="<?php echo $data[0]->first_name ?>">
                        </div>
                        <div class=" form-group">
                            <label for="lastName">Apellidos</label>
                            <input id="lastName" type="text" class="form-control" placeholder="Ingrese Apellidos" value="<?php echo $data[0]->last_name ?>">
                        </div>
                        <div class="form-group">
                            <button id="btnUpdateDriver" type="submit" class="btn btn-success">Guardar <i class="fas fa-save fa-lg"></i></button>
                            <a href="?controller=driver" class="btn btn-secondary">Cancelar <i class="fas fa-window-close"></i></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>